
class ListeEyetracker extends React.Component{
	constructor(props) {
	    super(props)
	    this.state = { 
	    	tab: [],
	    	charger:false,
	    	click:false
	    }
	 }

	changeTab=(e)=> {
		this.setState({charger:true,click:true})
		var spawn = require('child_process').spawn,
  		py = spawn('python', [__dirname+'/Python/List.py']);
  		var tempo;
		
		io.on('connection', function (socket) {
		    socket.on('calibration', function (data) {
		    	this.setState({tab:data,click:false})
		    }.bind(this));
		}.bind(this))

		setTimeout(()=>{py.kill()},10000)
	}

	render(){
		var option=[<option value="test" key="nullOption">null</option>];

		var buttonNoClick = 
			<button className="btn btn-primary btn-sm" type="button" onClick={this.changeTab}>
				  recherche
			</button>;
		var buttonClick = 
			<button className="btn btn-primary btn-sm" type="button">
				<span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
			</button>;
		(this.state.tab.length > 0)? option = []:null;

  		/*py.stdout.on('data', function(data){
		  console.log(data.toString())
		});*/

		for(var i =0;i<this.state.tab.length;i++){
			option.push(<option key={i} value={this.state.tab[i][1]}>{this.state.tab[i][0] +" | "+ this.state.tab[i][1]}</option>)
		}



		return(
			<div>
				<label htmlFor="eyetrackerSource">Eyetracker source: </label>
				<select style={{marginRight: 10 +"px",marginLeft: 10+"px"}} id={"Eyetracker"+this.props.salle}>
				{this.state.charger? null : this.changeTab()}
				{option}
				</select>
				{this.state.click ? buttonClick:buttonNoClick}
			</div>
		)
	}
}

ReactDOM.render(
	<ListeEyetracker salle="Salle1"/>,
	document.getElementById('listeEyeTrSalle1')
);
ReactDOM.render(
	<ListeEyetracker salle="Salle2"/>,
	document.getElementById('listeEyeTrSalle2')
);
ReactDOM.render(
	<ListeEyetracker salle="Salle3"/>,
	document.getElementById('listeEyeTrSalle3')
);
/************************************************************************
************************************************************************/
class ButtonExpe extends React.Component{
	render(){
		return(
			<button type="button" id="HomeButtonCommencerExp" className="btn btn-outline-success btn-lg btn-block" onClick={this.props.clickGazeStart}>Commencer l'expérience</button>
		)
	}
}

/************************************************************************
************************************************************************/

class ButtonStopExpe extends React.Component{
	render(){
		return(
			<button type="button" id="HomeButtonStopperExp" className="btn btn-outline-danger btn-lg btn-block" onClick={this.props.clickGazeStop}>Stopper l'expérience</button>
		)
	}
}

/************************************************************************
************************************************************************/


class EyetrackerCheckbox extends React.Component{
	constructor(props) {
	    super(props)
	    this.state ={
	    	id: this.generate(),
	    	checked: false,
	    	nomSalle:{salle: this.props.salle, salle_: this.props.salle_}
	    }
	}

	changeCheck=(e)=>{
		var checked;
		(this.state.checked)?this.setState({checked:false}):this.setState({checked:true});
		(this.state.checked)?checked=false:checked=true;
		(checked)?this.props.checkSalle(this.state.nomSalle):this.props.deCheckSalle(this.state.nomSalle);
	}

	generate(){
			var generator = require('generate-password');
	 
			var password = generator.generate({
			    length: 10,
			    numbers: true
			});
			return password
	}


	render(){
		return(
			<div className="custom-control custom-checkbox d-flex justify-content-center">
			 <input type="checkbox" className="custom-control-input" onChange={this.changeCheck} id={this.state.id} disabled={this.props.disabled}/>
			 <label className="custom-control-label" htmlFor={this.state.id}>{"Eyetracker "+this.props.salle}</label>
			</div>
		)
	}
}

/***********************************************************************

***********************************************************************/

class PointsCalibrage extends React.Component{
	render(){
		return(
			<div className="d-flex justify-content-center">
				<div className={(this.props.selectBox)?"card text-white mb-3 bg-success":"card mb-3"} style={{maxWidth:'320px'}}>
				  <div className="card-header text-center">{this.props.nbrPoints+" points"}</div>
				  <div className="card-body d-flex justify-content-center">
				  	<img src={"media/"+this.props.nbrPoints+"_points.png"}/>
				  </div>
				</div>
			</div>
		)
	}
}

/***********************************************************************

***********************************************************************/
class NombreDePointsCalibrage extends React.Component{
	constructor(props){
		super(props)
		this.state={
			select:this.props.defaultValue,
		}
	}
	click=(i,e)=>{
		this.props.funcChange(this.props.salle[this.props.salle.length-1]-1,i)
		this.setState({select:i})
	}

	verif=(nbr)=>{
	
			if(this.state.select==nbr){
				return true
			}
			
			return false
	}

	/*componentDidUpdate(prevProps) {
		  if (this.props.choixCalibration !== prevProps.choixCalibration) {
		  	console.log(this.props.choixCalibration+ " : "+ prevProps.choixCalibration )
		    // this.setState({select:this.props.choixCalibration})
		  }
	}
*/

	render(){

		return(
			<div>
				<div className="card mb-3">
				  <div className="card-header text-center">{this.props.salle}</div>
				  	<div className="card-body">
						<div className="row">
							<div className="col-sm" onClick={(e)=> this.click(3,e)}>
								<PointsCalibrage nbrPoints="3" selectBox={this.verif(3)}/>
							</div>
							<div className="col-sm" onClick={(e)=> this.click(5,e)}>
								<PointsCalibrage nbrPoints="5" selectBox={this.verif(5)}/>
							</div>
							<div className="col-sm" onClick={(e)=> this.click(9,e)}>
								<PointsCalibrage nbrPoints="9" selectBox={this.verif(9)}/>
							</div>
						</div>
					</div>
				</div>
				<br/>
			</div>
		)
	}
}
/***********************************************************************
***********************************************************************/
class ButtonLancementCalibrage extends React.Component{
	constructor(props) {
	    super(props)
	    this.state = { 
	    	nbrClick:[0,0,0],
	    	pyIsLive:[false,false,false],
	    	python: [null,null,null],
	    	verif:[false,false,false],
	    	salles:[],
	    	timestamp:[],
	    	expeStart:false,
	    	generalClickCali:0,
	    	choixCalibration:[null,null,null],
	    	nombreDePointsCalibrage:[null,null,null],
	    }
	}

	python=(e)=>{
		var nbr=0
		this.state.choixCalibration.forEach((element)=>{
			(element!=null)?nbr+=1:null;
		})
		if(this.state.salles.length!==nbr){
			alert("Sélectionner le nombre de points de calibration !")
			return;
		}
		this.setState({generalClickCali:this.state.generalClickCali+1})
  		for(var i=0;i<this.state.salles.length;i++){
  			(this.state.salles[i].salle==="salle1")?this.caliSalle(this.state.salles[i],0):(this.state.salles[i].salle==="salle2")?this.caliSalle(this.state.salles[i],1):this.caliSalle(this.state.salles[i],2);
  		}
	}

	caliSalle(salle,id){
			
			if(this.state.python[id]!==null){
				this.state.python[id].kill();
				this.setState({pyIsLive:[false,false,false],python:[null,null,null]});
				var { ipcRenderer } = require('electron')
				ipcRenderer.send('close_Calibrage'+salle.salle_, 'yes');
			}
			var tempo_nbrClick = this.state.nbrClick;
			tempo_nbrClick[id] = this.state.nbrClick[id]+1;
			
			this.setState({nbrClick:tempo_nbrClick})

			var {exec,spawn} = require('child_process');

			if(!this.state.pyIsLive[id]){
				var tempo_pyIsLive=this.state.pyIsLive
  				var tempo_python=this.state.python

  				tempo_pyIsLive[id]=true
  				tempo_python[id]=spawn('python', [__dirname+'/Python/main_'+salle.salle+'.py'])

	  			this.setState({python:tempo_python,pyIsLive : tempo_pyIsLive});
	  		}
	  		
			if (this.state.nbrClick[id]<=1){
				
		  		io.on('connection', function (socket) {
				    socket.on('searchEyetracker'+salle.salle_, function (data) {
				    	var tempo_verif = this.state.verif;
				    	tempo_verif[id]=data['eyetrackerTrue'];

				    	this.setState({verif: tempo_verif})

				    	if(this.state.verif[id]){
				  			console.log(this.state.verif[id])
				  			var { ipcRenderer } = require('electron')
					  		ipcRenderer.send('open_Calibrage'+salle.salle_, 'yes')
				  		}
				    }.bind(this));
				    socket.on('leave_calibration'+salle.salle_, function (data) {
				    	console.log(data)
				    	var { ipcRenderer } = require('electron')
				    	ipcRenderer.send('close_Calibrage'+salle.salle_, 'yes');
				    	/*var csv = require('fast-csv')
				    	var tabCSV =[];
				    	csv.parseFile(fileSave+'\\saved_calibration_'+salle.salle+'.csv').on('data', (row) => {
				    		(row.length>0)?tabCSV.push(row.toString()):null;
				    	})*/
				    	const open = require('open');
				    	(async () => {
    						await open(fileSave+'\\saved_calibration_'+salle.salle+'.csv', {wait: false}); 
						})();
				    	/*alert(JSON.stringify(tabCSV))
				    	var { dialog } = require('electron').remote
						dialog.showMessageBox({type:"info",title:"Résultat Calibration "+salle.salle,message: tabCSV.join('\n')})*/
				    }.bind(this));
				}.bind(this))
	  		}
	  		setTimeout(()=>{io.emit("TrackerId"+salle.salle_,{tr: $("#Eyetracker"+salle.salle_).val(),ptsCali: this.state.choixCalibration[id],url:fileSave})},1000)
  	}

  	checkSalle=(salle)=>{
  		var tempo = this.state.salles,tempo_nbrClick=this.state.nbrClick,tempo_pyIsLive=this.state.pyIsLive,tempo_python=this.state.python,tempo_verif=this.state.verif;

  		tempo.push(salle);
  	
  		this.setState({salles: tempo})


  		/*********************************************************************************************************/
  		
  		var nombreDePointsCalibrage = this.state.nombreDePointsCalibrage
		var salles = this.state.salles;

		salles.sort((a, b)=>{
		  	return a.salle_[a.salle_.length-1] - b.salle_[b.salle_.length-1];
		});

		salles.forEach((element,index) => {
			var generator = require('generate-password');
			var password = generator.generate({length: 10,numbers: true});
			nombreDePointsCalibrage[index] = <NombreDePointsCalibrage salle={element.salle_} key={password} funcChange={this.nbrCalibration} defaultValue={this.state.choixCalibration[element.salle_[element.salle_.length-1]-1]}/>;
		})

		this.setState({nombreDePointsCalibrage: nombreDePointsCalibrage})

		this.forceUpdate();

  	}

  	deCheckSalle=(salle)=>{
  		for(var i = 0;i < this.state.salles.length;i++){
  			if(salle.salle == this.state.salles[i].salle && salle.salle_ == this.state.salles[i].salle_){
  				var tempo = this.state.salles,tempo_nbrClick=this.state.nbrClick,tempo_pyIsLive=this.state.pyIsLive,tempo_python=this.state.python,tempo_verif=this.state.verif;

  				tempo.splice(i, 1)
  				

  				this.setState({salles: tempo})
  			}
  		}

  		/*********************************************************************************************************/

  		var nombreDePointsCalibrage = this.state.nombreDePointsCalibrage
		var salles = this.state.salles;

		salles.sort((a, b)=>{
		  	return a.salle_[a.salle_.length-1] - b.salle_[b.salle_.length-1];
		});

		nombreDePointsCalibrage.forEach((element,index) => {
			nombreDePointsCalibrage[index] = null;
		})
		var tabNull=[null,null,null]
		this.setState({nombreDePointsCalibrage: nombreDePointsCalibrage})
		this.setState({choixCalibration:tabNull})

		salles.forEach((element,index) => {
			var generator = require('generate-password');
			var password = generator.generate({length: 10,numbers: true});
			nombreDePointsCalibrage[index] = <NombreDePointsCalibrage salle={element.salle_} key={password} funcChange={this.nbrCalibration} defaultValue={null}/>;
		})

		this.setState({nombreDePointsCalibrage: nombreDePointsCalibrage})


  	};

  	goGazeEmit(salle){
  		console.log(salle)
  		io.emit("gaze_data"+salle.salle_,{gaze_data:"Go"})
  	}

  	stopGazeEmit(salle){
  		console.log(salle)
  		io.emit("stop_gaze_data"+salle.salle_,{stop_gaze_data:"stop",url:fileSave})
  	}

	gaze=(e)=>{
		for(var i=0;i<this.state.salles.length;i++){
  			this.goGazeEmit(this.state.salles[i])
  		}
  		var tempo= this.state.timestamp
		startRecording_salle1(tempo);
        startRecording_salle2(tempo);
        startRecording_salle3(tempo);
        startRecording_salle1_Ecran();
        startRecording_salle2_Ecran();
        startRecording_salle3_Ecran();
        this.setState({timestamp:tempo,expeStart:true})
        console.log(this.state.timestamp)
	};

	saveToCSV=(data)=>{
		var objData=[]

		for(var i = 0;i < data.length; i++){
			objData.push({key:Object.keys(data[i])[0],timestamp:Object.values(data[i])[0]})
		}
		
		const ObjectsToCsv = require('objects-to-csv');

		(async() =>{
		  let csv = new ObjectsToCsv(objData);
		  await csv.toDisk(fileSave+'/data_timestamp_video.csv');	 
		})();

	}

	stopGaze=(e)=>{
		for(var i=0;i<this.state.salles.length;i++){
  			this.stopGazeEmit(this.state.salles[i])
  		}
  		var tempo= this.state.timestamp
  		this.setState({timestamp:[],expeStart:false})
		stopRecording_salle1(tempo);
        stopRecording_salle2(tempo);
        stopRecording_salle3(tempo);
        stopRecording_salle1_Ecran();
        stopRecording_salle2_Ecran();
        stopRecording_salle3_Ecran();
        (tempo.length >= 6)?this.saveToCSV(tempo):null;
        console.log(tempo)

        //setTimeout(()=>{this.state.python.kill();this.setState({pyIsLive:false})},10000)
	};

	nbrCalibration=(nbrSalle,nbrCallibration)=>{
		var tempo = this.state.choixCalibration;
		tempo[nbrSalle]=nbrCallibration;
		this.setState({choixCalibration:tempo});
		console.log(this.state.choixCalibration);
	}
	
	render(){
		var nombreDePointsCalibrage=[];

		this.state.nombreDePointsCalibrage.forEach((element)=>{
			(element!==null)?nombreDePointsCalibrage.push(element):null;
		})
		return(
			<div>
				<br/>
				<div className="card">
					<div className="card-header text-center"> 
						<h5>Calibrage</h5>
						<div className="row">
						    <div className="col-sm">
						      	<EyetrackerCheckbox salle="salle1" salle_="Salle1" checkSalle={this.checkSalle} deCheckSalle={this.deCheckSalle} disabled={(this.state.generalClickCali%2==0)?false:true}/>
						    </div>
						    <div className="col-sm">
						      	<EyetrackerCheckbox salle="salle2" salle_="Salle2" checkSalle={this.checkSalle} deCheckSalle={this.deCheckSalle} disabled={(this.state.generalClickCali%2==0)?false:true}/>
						    </div>
						    <div className="col-sm">
						      	<EyetrackerCheckbox salle="salle3" salle_="Salle3" checkSalle={this.checkSalle} deCheckSalle={this.deCheckSalle} disabled={(this.state.generalClickCali%2==0)?false:true}/>
						    </div>
						</div>
					</div>
	            	<div className="card-body">
						{nombreDePointsCalibrage}
						<button type="button" className="btn btn-outline-success btn-lg btn-block" onClick={this.python} disabled={(this.state.salles.length > 0)? false : true}>{(this.state.generalClickCali%2==0)?"Lancer le Calibrage":"Fermer le Calibrage"}</button>
					</div>
				</div>
				<br/>
				<div>
					{(this.state.generalClickCali>0 || this.state.salles.length <= 0)? (this.state.expeStart)?<ButtonStopExpe clickGazeStop={this.stopGaze}/> : <ButtonExpe clickGazeStart={this.gaze}/> : null}
				</div>
			</div>
		)
	}
}

ReactDOM.render(
	<ButtonLancementCalibrage/>,
	document.getElementById('buttonLancementCalibrage')
);

/************************************************************************
************************************************************************/

class EffetInteractMiroir extends React.Component{
	
	select=(e)=>{
		io.emit("EffetInteractMiroir"+this.props.salle, { select: e.target.value});
	}

	render(){
		return(
			<div className="form-group">
			    <label htmlFor={"EffetInteractMiroir"+this.props.salle}>Select effet:</label>
			    <select className="form-control" id={"EffetInteractMiroir"+this.props.salle} onChange={this.select}>
			      <option value="Interaction">Effet Interaction</option>
			      <option value="Miroir">Effet Miroir</option>
			    </select>
			</div>		
		)
	}
}

ReactDOM.render(
	<EffetInteractMiroir salle="Salle1"/>,
	document.getElementById('ButtonLancementCalibrageSalle1')
);
ReactDOM.render(
	<EffetInteractMiroir salle="Salle2"/>,
	document.getElementById('ButtonLancementCalibrageSalle2')
);
ReactDOM.render(
	<EffetInteractMiroir salle="Salle3"/>,
	document.getElementById('ButtonLancementCalibrageSalle3')
);