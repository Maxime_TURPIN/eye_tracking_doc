class Edit extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			down: true,
			numSalle: 1,
			salle1:[],
			salle2:[],
			salle3:[],
			edit:false,
		};
	}

	handleClick=(e)=>{
		var tempo;
		this.state.down? tempo=false:tempo=true;
		this.setState({down:tempo})
	}

	setNativeValue(element, value){
	  const valueSetter = Object.getOwnPropertyDescriptor(element, 'value').set;
	  const prototype = Object.getPrototypeOf(element);
	  const prototypeValueSetter = Object.getOwnPropertyDescriptor(prototype, 'value').set;
	  
	  if (valueSetter && valueSetter !== prototypeValueSetter) {
	  	prototypeValueSetter.call(element, value);
	  } else {
	    valueSetter.call(element, value);
	  }
	}

	verif=(salle,o)=>{
		var x = [];
		var y =[];
		var name=null;
		var nbrVideo=0
		var scenarioVideo=[]
		var nbrImage=0
		var scenarioImage=[]
		var nbrDiffusion=0
		var scenarioDiffusion=[]
		for (var i =0; i<o.video.length;i++){
			(o.video[i][0]===null)?o.video.splice(i,1):null;
		}
		for (var i =0; i<o.video.length;i++){
			(o.video[i][0]!==null)?x.push(o.video[i][0].indexOf(salle)):x.push(-1);
		}
		for (var i =0; i<o.video.length;i++){
			(x[i]!==-1)?y.push(i):null;
		}
		y.forEach((ele)=>{
			(o.video[ele][0].indexOf("vidéo")!==-1)?nbrVideo+=1:null;
			(o.video[ele][0].indexOf("image")!==-1)?nbrImage+=1:null;
			(o.video[ele][0].indexOf("diffusion")!==-1)?nbrDiffusion+=1:null;
		})
		for (var i =0; i<Math.trunc(nbrVideo/7);i++){scenarioVideo.push(<ScenarioRangeInputPosition typeRangeScenario="Video" typeRangeScenario2="vidéo" numSalle={salle} key={"ScenarioRangeInputPosition"+i}/>)}
		for (var i =0; i<Math.trunc(nbrImage/7);i++){scenarioImage.push(<ScenarioRangeInputPosition typeRangeScenario="Image" typeRangeScenario2="image" numSalle={salle} key={"ScenarioRangeInputPosition"+i}/>)}
		for (var i =0; i<Math.trunc(nbrDiffusion/4);i++){scenarioDiffusion.push(<ScenarioDiffusion numSalle={salle}/>)}
		return scenarioVideo.concat(scenarioImage.concat(scenarioDiffusion))
	}

	edit=()=>{
		this.setState({edit:true})

		var storage = require('electron-json-storage');
 		storage.setDataPath(__dirname+"/../Storage/scenar")
 		storage.get(this.props.file, function(error, data){
		  if (error) throw error;
			var json = data
			var o=JSON.parse(json)

			this.setState({salle1:this.verif("salle1",o)})
			this.setState({salle2:this.verif("salle2",o)})
			this.setState({salle3:this.verif("salle3",o)})
			
			var class_ = document.getElementById('EditScenarModal').getElementsByClassName('storage')
			
			setTimeout(()=>{
			for (var i = 0; i < class_.length; i++) {
			    (class_[i].tagName!=="SELECT" && class_[i].tagName!=="BUTTON")?this.setNativeValue(class_[i], o.video[i][1]):class_[i].value=o.video[i][1];
			    (class_[i].tagName==="BUTTON")?class_[i].innerHTML=o.video[i][1]:null;
			    //{target:{name:"test",value:o.video[i][1]}}
			    var event = new Event('change',{ bubbles: true })
			    class_[i].dispatchEvent(event)
			}},500)
		}.bind(this));
	}

	

	render(){
		var chevronBottom = <path d="M1.5 1l-1.5 1.5 4 4 4-4-1.5-1.5-2.5 2.5-2.5-2.5z"></path>
		var chevronRight = <path d="M2.5 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z"></path>
		return(
			<React.Fragment>
			{(this.state.edit===false)?this.edit():null}
			<div className="card">
                <h5 className="card-header" onClick={this.handleClick}><svg width="15px" viewBox="0 0 8 8" className={"animated"+(this.state.down?" rotateInDownLeft ":" rotateInDownRight ")+"slow"}>{this.state.down?chevronBottom:chevronRight}</svg> {" Scénario pour la salle 1"}</h5>
                <div className="card-body" hidden={!this.state.down}>
	                {this.state.salle1}
                </div>
            </div>
            <div className="card">
                <h5 className="card-header" onClick={this.handleClick}><svg width="15px" viewBox="0 0 8 8" className={"animated"+(this.state.down?" rotateInDownLeft ":" rotateInDownRight ")+"slow"}>{this.state.down?chevronBottom:chevronRight}</svg> {" Scénario pour la salle 2"}</h5>
                <div className="card-body" hidden={!this.state.down}>
	                {this.state.salle2}
                </div>
            </div>
            <div className="card">
                <h5 className="card-header" onClick={this.handleClick}><svg width="15px" viewBox="0 0 8 8" className={"animated"+(this.state.down?" rotateInDownLeft ":" rotateInDownRight ")+"slow"}>{this.state.down?chevronBottom:chevronRight}</svg> {" Scénario pour la salle 3"}</h5>
                <div className="card-body" hidden={!this.state.down}>
	                {this.state.salle3}
                </div>
            </div>
            </React.Fragment>
		)
	}

}