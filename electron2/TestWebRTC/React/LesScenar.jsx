class LesScenar extends React.Component{
	constructor(props) {
	    super(props);
	    this.state = { 
	    	search:false,
	    	files:[],
	    	dates:[],
	    	file:"",
	    	data:"",
	    	edit:null,
	    	key:0,
	    };
  	}

	search=()=>{
		//requiring path and fs modules
			this.setState({search:true})
			const path = require('path');
			const fs = require('fs');
			//joining path of directory 
			const directoryPath = __dirname+"/../Storage/scenar/";
			//passsing directoryPath and callback function
			fs.readdir(directoryPath, function (err, files) {
			    //handling error
			    if (err) {
			        return console.log('Unable to scan directory: ' + err);
			    } 
			    (files.length!=0)?this.setState({files:files}):null;
			    (this.state.file==="")?this.setState({file:files[0]}):null;
			    /*//listing all files using forEach
			    files.forEach(function (file) {
			        // Do whatever you want to do with the file
			        console.log(file); 
			    });*/
			}.bind(this));
			this.state.files.forEach((element)=>{
				fs.stat(directoryPath+"/"+element, function(err, stats){
				    var tempo = this.state.dates;tempo.push(stats.mtime.toString())
				    this.setState({dates:tempo})
				}.bind(this));
			})
			setTimeout(()=>{this.setState({search:false})},5000)
	}

	edit=(e,file)=>{
		$('#EditScenarModal').modal('show');
		this.setState({file:file,edit:<Edit key={this.state.key} file={file}/>,key:this.state.key+1})
	}

	trash=(e,file)=>{
		var storage = require('electron-json-storage');
 		storage.setDataPath(__dirname+"/../Storage/scenar")
		storage.remove(file, function(error) {
		  if (error) throw error;
		});
	}

	close=(e)=>{
		this.setState({edit:null})
	}

	render(){
		(!this.state.search)?this.search():null;
		var tbody=[]


		this.state.files.forEach((element,index)=>{
			var trash= <svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
  							<path d="M3 0c-.55 0-1 .45-1 1h-1c-.55 0-1 .45-1 1h7c0-.55-.45-1-1-1h-1c0-.55-.45-1-1-1h-1zm-2 3v4.81c0 .11.08.19.19.19h4.63c.11 0 .19-.08.19-.19v-4.81h-1v3.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-3.5h-1v3.5c0 .28-.22.5-.5.5s-.5-.22-.5-.5v-3.5h-1z" />
						</svg>;
			var pen = <svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
						  <path d="M6 0l-1 1 2 2 1-1-2-2zm-2 2l-4 4v2h2l4-4-2-2z" />
						</svg>;
			var button=<tr key={index} className="text-center"><th scope="row">{index}</th><td>{element}</td><td>{this.state.dates[index]}</td><td><button type="button" className="btn btn-outline-info btn-sm icon-grid" value={element} onClick={(e)=>{this.edit(e,element)}}>{pen}</button> <button type="button" className="btn btn-outline-danger btn-sm icon-grid" onClick={(e)=>{this.trash(e,element)}}>{trash}</button></td></tr>;
			tbody.push(button)
		})
		return(
			<React.Fragment>
			<div className="container-fluid">
			    <div className="card">
			        <h5 className="card-header text-center">Les Scénarios</h5>
				    <div className="card-body">
				    	<table className="table">
						  	<thead className="thead-dark">
							    <tr className="text-center">
							      <th scope="col">#</th>
							      <th scope="col">Name</th>
							      <th scope="col">Date</th>
							      <th></th>
							    </tr>
						  	</thead>
							<tbody>	
								{tbody}		    
				    		</tbody>
						</table>
			    	</div>
			    </div>
			</div>

			<div className="modal fade" id="EditScenarModal" tabIndex="-1" role="dialog" aria-hidden="true">
			  <div className="modal-dialog modal-xl">
			    <div className="modal-content">
			    	<div className="modal-header">
				        <h5 className="modal-title" >{"Edit "+this.state.file}</h5>
				        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.close}>
				          <span aria-hidden="true">&times;</span>
				        </button>
			      	</div>
			      	<div className="modal-body">
			        	{this.state.edit}
			      	</div>
			      	<div className="foot-body">
			      		<Storage id="EditScenarModal" fileName={this.state.file}/>
			      	</div>
			    </div>
			  </div>
			</div>
			</React.Fragment>
		)
	}	
}

ReactDOM.render(
	<LesScenar/>,
	document.getElementById('LesScenar')
);