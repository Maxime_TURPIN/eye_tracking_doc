class ScenarioHtmlDiffusionDirect extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			pts: 0,
			moment:0,
			duree:0,
			camera:null,
		}
	}

	handleChange=(e)=>{
		this.setState({pts:e.target.value})
	}

	onClickEnvoyer=(e)=>{
		io.emit(this.props.salle_+'_stream_latence', {moment:this.state.moment,duree:this.state.duree, id:this.generate(),id_2:this.generate(),latence: this.state.pts,camera:this.state.camera})
	}
	moment=(e)=>{
		this.setState({moment:e.target.value})
	}

	duree=(e)=>{
		this.setState({duree:e.target.value})
	}

	generate(){
			var generator = require('generate-password');
	 
			var password = generator.generate({
			    length: 20,
			    numbers: true
			});
			return password
	}

	onChange=(e)=>{
		var _Salle_=this.props.salle_
        var _output_;
        "Salle1"==_Salle_?_output_=audioOutputSelect.value:"Salle2"==_Salle_?_output_=audioOutputSelect2.value:_output_=audioOutputSelect3.value;
        if(e.target.value=="cameraSalle1"){io.emit(_Salle_+'_stream', {video: videoSource.value,audio: audioInputSelect.value, output: _output_});};
        if(e.target.value=="cameraSalle2"){io.emit(_Salle_+'_stream', {video: videoSource2.value, audio: audioInputSelect2.value ,output: _output_});};
        if(e.target.value=="cameraSalle3"){io.emit(_Salle_+'_stream', {video: videoSource3.value,audio: audioInputSelect3.value,output: _output_});};
        (e.target.value=="cameraSalle1")?this.setState({camera:0}):(e.target.value=="cameraSalle2")?this.setState({camera:2}):this.setState({camera:3});
	}

	render(){

		/*var customSwitch=
		
			<div className="custom-control custom-switch">
				<input type="checkbox" className="custom-control-input" id={this.props.idToggle[this.props.idToggle.length-1]+this.props.numSalle} onClick={this.props.handleDisabledDureeAndButtADD} />
			</div>
		
		;*/

		return(
			<div className={this.props.animate}>
				<h5 className="card-title">Diffusion</h5>
				<div>
					<div className="card">
						<div className="card-body">
							<label className="my-1 mr-2">{"Diffuser sur l'écran de la "+this.props.salle}</label>
							<select className="custom-select my-1 mr-sm-2" name={"select diffusion "+this.props.salle} onChange={this.onChange}>
								<option value="cameraSalle1" selected>la camera de la salle 1</option>
								<option value="cameraSalle2">la camera de la salle 2</option>
								<option value="cameraSalle3">la camera de la salle 3</option>
							</select>
							<div className="row">
								<label>Une différence de {this.state.pts} milliseconde</label>
								<input type="range" className="custom-range" name={"différence diffusion salle"+this.props.numSalle} defaultValue="0" min="0" max="30000" step="1" onChange={this.handleChange} />
								<div className="col">
									<div className="card">
										<div className="card-body">
											<label>Durée (en ms)</label>
											<input type="number" className="form-control" name={"duree diffusion "+this.props.numSalle} defaultValue="1000" step="1" onChange={this.duree}/>
										</div>
									</div>
								</div>
								<div className="col">
									<label>A quel moment ? (en ms)</label>
									<input type="number" className="form-control" name={"moment diffusion salle"+this.props.numSalle} defaultValue="500" step="1" onChange={this.moment}/>
								</div>
							</div>
							<br/>
							<div>
								<button type="button" className="btn btn-success" onClick={this.onClickEnvoyer}>Envoyer</button>
							</div>
						</div>
					</div> 
				</div>
			</div>
		)
	}
}

class ScenarioDiffusionDirect extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			nbr: 1,
			idToggle: ["test","test1"],
			animate: "animated zoomIn fast",
			nbrTimeout: 0,
			DisaDureeAndButtADD: false,
		}
	}

	render(){
		return(
			<div>
				<ScenarioHtmlDiffusionDirect salle="salle 1" salle_="Salle1" animate={this.state.animate} />
				<ScenarioHtmlDiffusionDirect salle="salle 2" salle_="Salle2" animate={this.state.animate} />
				<ScenarioHtmlDiffusionDirect salle="salle 3" salle_="Salle3" animate={this.state.animate} />
			</div>
		)
	}
}

ReactDOM.render(
	<ScenarioDiffusionDirect/>,
	document.getElementById('Scenario_direct_lattence')
);