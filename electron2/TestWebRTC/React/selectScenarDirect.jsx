class SelectScenar extends React.Component{

	constructor(props) {
	    super(props);
	    this.state = { 
	    	search:false,
	    	files:[],
	    	file:"",
	    	data:"",
	    };
  	}

	search=()=>{
		//requiring path and fs modules
			this.setState({search:true})
			const path = require('path');
			const fs = require('fs');
			//joining path of directory 
			const directoryPath = __dirname+"/../Storage/scenar/";
			//passsing directoryPath and callback function
			fs.readdir(directoryPath, function (err, files) {
			    //handling error
			    if (err) {
			        return console.log('Unable to scan directory: ' + err);
			    } 
			    (files.length!=0)?this.setState({files:files}):null;
			    (this.state.file==="")?this.setState({file:files[0]}):null;
			    /*//listing all files using forEach
			    files.forEach(function (file) {
			        // Do whatever you want to do with the file
			        console.log(file); 
			    });*/
			}.bind(this));
			setTimeout(()=>{this.setState({search:false})},5000)

	}

	edit=(json_)=>{
		this.setState({edit:true})
		var json = json_
		var o=JSON.parse(json)

		this.verif("salle1",o)
		this.verif("salle2",o)
		this.verif("salle3",o)
	}

	verif=(salle,o)=>{
		var x = [];
		var y =[];
		var name=null;
		var nbrVideo=0
		var scenarioVideo=[]
		var nbrImage=0
		var scenarioImage=[]
		var nbrDiffusion=0
		var scenarioDiffusion=[]
		for (var i =0; i<o.video.length;i++){
			(o.video[i][0]===null)?o.video.splice(i,1):null;
		}
		for (var i =0; i<o.video.length;i++){
			(o.video[i][0]!==null)?x.push(o.video[i][0].indexOf(salle)):x.push(-1);
		}
		for (var i =0; i<o.video.length;i++){
			(x[i]!==-1)?y.push(i):null;
		}
		y.forEach((ele)=>{
			(o.video[ele][0].indexOf("vidéo")!==-1)?scenarioVideo.push(o.video[ele]):null;
			(o.video[ele][0].indexOf("image")!==-1)?scenarioImage.push(o.video[ele]):null;
			(o.video[ele][0].indexOf("diffusion")!==-1)?scenarioDiffusion.push(o.video[ele]):null;
		})
		//var img = '<img src="'+.replace(/\\/g,"\\\\")+'" id="'+id+'" style="-webkit-transform: scaleX(-1);transform: scaleX(-1); position: fixed;right:'+HomeRight+'px;top:'+HomeTop+'px;" width="'+HomeWidth+'px" height="'+HomeHeight+'px">';
		console.log(scenarioVideo)
		console.log(scenarioImage)
		console.log(scenarioDiffusion)

		for (var i =0; i<scenarioVideo.length;i+=7){
			var id= generator.generate({length: 20,numbers: true});
	        var vid = '<video src="'+scenarioVideo[i][1].replace(/\\/g,"\\\\")+'" id="'+id+'" style="-webkit-transform: scaleX(-1);transform: scaleX(-1); position: fixed;right:'+scenarioVideo[i+1][1]+'px;top:'+scenarioVideo[i+3][1]+'px;" width="'+scenarioVideo[i+2][1]+'px" height="'+scenarioVideo[i+4][1]+'" autoplay muted></video>';
			var salle_=null;
			(salle==="salle1")?salle_="Salle1":(salle==="salle2")?salle_="Salle2":salle_="Salle3";
			io.emit(salle_+'_img_video', { content: vid, duree: scenarioVideo[i+5][1], moment: scenarioVideo[i+6][1],id: id});
		}
		for (var i =0; i<scenarioImage.length;i+=7){
			var id= generator.generate({length: 20,numbers: true});
	        var img = '<img src="'+scenarioImage[i][1].replace(/\\/g,"\\\\")+'" id="'+id+'" style="-webkit-transform: scaleX(-1);transform: scaleX(-1); position: fixed;right:'+scenarioImage[i+1][1]+'px;top:'+scenarioImage[i+3][1]+'px;" width="'+scenarioImage[i+2][1]+'px" height="'+scenarioImage[i+4][1]+'"></img>';
			var salle_=null;
			(salle==="salle1")?salle_="Salle1":(salle==="salle2")?salle_="Salle2":salle_="Salle3";
			io.emit(salle_+'_img_video', { content: img, duree: scenarioImage[i+5][1], moment: scenarioImage[i+6][1],id: id});
		}
		for (var i =0; i<scenarioDiffusion.length;i+=4){
			var salle_=null;
			(salle==="salle1")?salle_="Salle1":(salle==="salle2")?salle_="Salle2":salle_="Salle3";
			 var _output_;
        	"Salle1"==salle_?_output_=audioOutputSelect.value:"Salle2"==salle_?_output_=audioOutputSelect2.value:_output_=audioOutputSelect3.value;		
			if(scenarioDiffusion[i][1]=="cameraSalle1"){setTimeout(()=>{io.emit(salle_+'_stream', {video: videoSource.value,audio: audioInputSelect.value, output: _output_});},scenarioDiffusion[i+2][1])};
        	if(scenarioDiffusion[i][1]=="cameraSalle2"){setTimeout(()=>{io.emit(salle_+'_stream', {video: videoSource2.value, audio: audioInputSelect2.value ,output: _output_});},scenarioDiffusion[i+2][1])};
        	if(scenarioDiffusion[i][1]=="cameraSalle3"){setTimeout(()=>{io.emit(salle_+'_stream', {video: videoSource3.value,audio: audioInputSelect3.value,output: _output_});},scenarioDiffusion[i+2][1])};
        	var camera;
        	(scenarioDiffusion[i][1]=="cameraSalle1")?camera=0:(scenarioDiffusion[i][1]=="cameraSalle2")?camera=2:camera=3;
        	if(scenarioDiffusion[i+3][1]!="0"){
        		io.emit(salle_+'_stream_latence', {moment:scenarioDiffusion[i+2][1],duree:scenarioDiffusion[i+1][1], id:generator.generate({length: 20,numbers: true}),id_2:generator.generate({length: 20,numbers: true}),latence: scenarioDiffusion[i+3][1],camera:camera})
        	}
		}
		
		/*for (var i =0; i<Math.trunc(scenarioImage.length);i+=7){console.log(o.video[7])}
		for (var i =0; i<Math.trunc(scenarioDiffusion.length);i+=4){console.log(o.video[1])}*/
	}

	click=(e)=>{
		var json = null;

		const storage = require('electron-json-storage');
 		storage.setDataPath(__dirname+"/../Storage/scenar")
		storage.get(this.state.file, function(error, data){
		  if (error) throw error;
		  	this.setState({data:data})
    		this.edit(this.state.data)
		}.bind(this));
	}

	change=(e)=>{
		this.setState({file:e.target.value})
	}

	render(){
		var option = <option value="null" key={0}>null</option>;
		(!this.state.search)?this.search():null;
		(this.state.files.length!=0)?option=[]:null;
		this.state.files.forEach(function (file,index) {
			option.push(<option key={index} value={file}>{file}</option>)
		});

		return(
			<React.Fragment>
			<br/>
			<div className="input-group mb-3">
		      <select className="custom-select" id="inputGroupSelect02" onChange={this.change}>
		      	<option selected>--</option>
		        {option}
		      </select>
		      <div className="input-group-append">
		        <label className="input-group-text" htmlFor="inputGroupSelect02">Scénario</label>
		      </div>
		      <div className="input-group-append">
		        <button className="btn btn-success" type="button" onClick={this.click}>Play</button>
		      </div>
    		</div>
			</React.Fragment>
		)
	}
}

ReactDOM.render(
	<SelectScenar />,
	document.getElementById('selectScanrioDirect')
);