class LancerModalVideoFu extends React.Component{

	constructor(props){
		super(props)
		this.state= {
			time1:0,
			time2:0,
			t:0,
			ariaValuenow:0,
			width:{width: "0%"},
			estimation:null,
			reste:null,
			start:null,
			filtre:this.filtre()
		}
	}

	filtre=()=>{
		if(this.props.filtre===1){return ['[0:v]scale=1920:1080[0scaled]','[0:a]volume=1[outa]','[1:v]scale=960:540[1scaled]','[0scaled]pad=1920:1620[0padded]','[0padded][1scaled]overlay=shortest=1:y=1080[output]']}
		if(this.props.filtre===2){return ['[0:v]scale=1920:1080[0scaled]','[0:a]volume=1[outa]','[1:v]scale=1920:1080[1scaled]','[0scaled]pad=1920:2160[0padded]','[0padded][1scaled]overlay=shortest=1:y=1080[output]']}
		if(this.props.filtre===3){return ['[0:v]scale=1920:1080[0scaled]','[0:a]volume=1[outa]','[1:v]scale=480:270[1scaled]','[0scaled]pad=1920:1080[0padded]','[0padded][1scaled]overlay=shortest=1:y=810[output]']}
	}

	writeTime = (milli) => {
      var heure = milli / (3600000);
      var minute = (heure - Math.trunc(heure))*60;
      var seconde = (minute - Math.trunc(minute))*60;
      return Math.trunc(heure)+" heure(s) "+Math.trunc(minute)+" minute(s) "+Math.trunc(seconde)+" seconde(s) "
    };

	lancer=(e)=>{
		const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
      	const ffmpeg = require('fluent-ffmpeg');
      	ffmpeg.setFfmpegPath(ffmpegPath);

		var tempo;
      	var test = ffmpeg().input(this.props.url.principale.toString()).input(this.props.url.secondaire.toString()).complexFilter(this.state.filtre).outputOptions(['-map [output]']).outputOptions(['-map [outa]']).on('codecData', function(data) {
	        tempo=(data.duration).split(':')
	        tempo=parseFloat(tempo[0])*60*60+parseFloat(tempo[1])*60+parseFloat(tempo[2])
	        this.setState({t:tempo})
	        console.log(data.duration)
	        console.log(this.state.t)
      	}.bind(this)).on('start', function() {
         this.setState({time1:Date.now(),start:true})
      	}.bind(this)).output("./Storage/video_combine/output"+moment().format('DD_MM_YYYY_h_mm_ss')+".mp4").on('end',function(){
          console.log('stop');
          this.setState({ariaValuenow:100,width:{width: "100%"},start:false})
      	}.bind(this)).on('progress',function(progress){
	        this.setState({time2:Date.now()});
	        var time=(this.state.time2-this.state.time1)
	        var xy = parseFloat((progress.timemark).split(':')[0])*60*60+parseFloat((progress.timemark).split(':')[1])*60+parseFloat((progress.timemark).split(':')[2]);
	        var y =xy;
	        var x = (y/this.state.t*100);
	        var estimation = time*100/x;
	        var reste = estimation - time;
	        this.setState({estimation:"Estimation: " + this.writeTime(estimation),reste:"Il reste: " + this.writeTime(reste)})
	        this.setState({ariaValuenow:x,width:{width: x+"%"}})
      	}.bind(this)).run();
	};

	render(){
		
		return(
			<React.Fragment>
				<hr/>
				<div className="modal-body">

	              <div>
	                <p>{this.state.estimation}</p>
	                <p>{this.state.reste}</p>
	              </div>

	              <div className="progress">
	                <div className="progress-bar progress-bar-animated" role="progressbar" aria-valuenow={this.state.ariaValuenow} aria-valuemin="0" aria-valuemax="100" style={this.state.width}></div>
	              </div>
	            </div>
	            <div className="modal-footer">
	              {(this.state.start===true || this.state.start===false)?null:<button type="button" className="btn btn-success" onClick={this.lancer} >Lancer</button>}
	              {(this.state.start===false)?<button type="button" className="btn btn-success" data-dismiss="modal" >OK</button>:null}
	            </div>
	        </React.Fragment>
		)
	}
}

class ChooseModalVideoFu extends React.Component{

	constructor(props){
		super(props)
		this.state= {
			style: [{backgroundColor: "#6FC292"},null,null],
			result: 1,
		}
	}

	onClick=(e,i)=>{
		var tempo = [null,null,null];
		tempo[i-1]={backgroundColor: "#6FC292"}
		this.setState({style:tempo,result:i})
		this.props.funcChoixFiltre(i)
	}

	render(){
		return(
			<React.Fragment>
				<hr/>
				<h5 style={{paddingLeft:"1rem"}}>Choix poss video</h5>
				<div className="row">
					<div className="col d-flex justify-content-center">
						<div className="card">
		                	<div className="card-body d-flex justify-content-center" style={this.state.style[0]} onClick={(e)=>{this.onClick(e,1)}}>
								<img src="media/posVideo1.png"/>
							</div>
						</div>
					</div>
					<div className="col d-flex justify-content-center">
						<div className="card">
		                	<div className="card-body d-flex justify-content-center" style={this.state.style[1]} onClick={(e)=>{this.onClick(e,2)}}>
								<img src="media/posVideo2.png"/>
							</div>
						</div>
					</div>				
					<div className="col d-flex justify-content-center">
						<div className="card">
		                	<div className="card-body d-flex justify-content-center" style={this.state.style[2]} onClick={(e)=>{this.onClick(e,3)}}>
								<img src="media/posVideo3.png"/>
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

class InputModalVideoFu extends React.Component{

	constructor(props) {
	    super(props)
	    this.state = { 
	    	url:null,
	    	styleColor:null,
	    	interval:null
	    }
	}

	onClick=(e)=>{
		(this.state.interval!==null)?clearInterval(this.state.interval):null;
		const { dialog } = require('electron').remote;
        var file = dialog.showOpenDialog({ properties: ['openFile'],filters: [
            { name: 'mp4', extensions: ['mp4'] }
        ]});
        (file!==undefined)?this.setState({url:file}):null;
        (file!==undefined)?this.props.funcUrl(this.props.idURL,file):null;


        this.setState({styleColor :{transition: "fill 1s ease", fill:"#82E0AA"}})
        var inter = setInterval(()=>{
	    	(this.state.styleColor.fill==="#6FC292")?this.setState({styleColor :{transition: "fill 1s ease-in",fill:"#82E0AA"}}):this.setState({styleColor : {transition: "fill 1s ease-in",fill:"#6FC292"}})
	    },1000)
        this.setState({interval:inter})
	}

	render(){
		var up =
		<svg xmlns="http://www.w3.org/2000/svg" width="100px" viewBox="0 0 20 17">
	        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
	    </svg>;
		var check =
		<svg xmlns="http://www.w3.org/2000/svg" style={this.state.styleColor} width="110px" viewBox="0 0 8 8" >	
	      	<path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm2 1.78l.72.72-3.22 3.22-1.72-1.72.72-.72 1 1 2.5-2.5z" />
	    </svg>;

		return(
			<div className="col">
	            <div className="modal-body">
	              <div className="card">
	                <div className="card-body">
	                  <h5 className="card-title">{this.props.title}</h5>
	                  <h6 className="card-subtitle mb-2 text-muted">{this.props.titleMuted}</h6>
	                  <div className="d-flex justify-content-center">
	                      
	                      {(this.state.url===null)?up:check}
                      </div>
                      <button type="button" className={"btn "+((this.state.url===null)?"btn-outline-danger":"btn-outline-success")+" btn-block"} onClick={this.onClick}><svg xmlns="http://www.w3.org/2000/svg" width="20" viewBox="0 0 8 8"><path d="M0 0v8h7v-4h-4v-4h-3zm4 0v3h3l-3-3z" /></svg>{(this.state.url===null)?"  Aucun fichier choisi":"  "+this.state.url.toString()}</button>
	                </div>
	               </div>
	            </div>
	        </div>
        )
	}
}


class ModalVideoFu extends React.Component{

	constructor(props) {
	    super(props)
	    this.state = { 
	    	clickSuivant:false,
	    	url:{principale:null, secondaire:null},
	    	clickSuivant2:false,
	    	choixFiltre:1,
	    }
	}

	funcChoixFiltre=(i)=>{
		this.setState({choixFiltre:i})
	};


	funcUrl=(id,url)=>{
		var urlTempo = this.state.url;
		urlTempo[id]=url
		this.setState({url1:urlTempo});
	};

	checkUrl=()=>{
		var t = true;
		Object.keys(this.state.url).forEach((element)=>{
				if(this.state.url[element]===null){
					t= false;
				}
		})
		return t
	};

	onClick=(e)=>{
		this.setState({clickSuivant:true})
	};

	onClickSuivant2=(e)=>{
		this.setState({clickSuivant2:true})
	};

	render(){
		

      	var suivant =
      		<React.Fragment>
	      		<br/>
					<div className="d-flex justify-content-end" style={{padding:"1rem"}}>
						<button className="btn btn-outline-success" onClick={this.onClickSuivant2}>Suivant</button>
					</div>
				<br/>
			</React.Fragment>;
		var buttonClose = <button type="button" className="close" data-dismiss="modal" aria-label="Close">
					        <span aria-hidden="true">&times;</span>
					      </button>

		return(	<div className="modal fade" id="ModalVideoFu" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			      	<div className="modal-dialog modal-dialog-centered modal-xl" role="document">
			        	<div className="modal-content">

			          		<div >
					            <div className="modal-header">
					              <h5 className="modal-title" >Choisir 2 videos</h5>
					              {(!this.state.clickSuivant2)?buttonClose:null}
					            </div>

					            <div className="row">
					              {(!this.state.clickSuivant2)?<InputModalVideoFu title="La video principale" titleMuted="Càd la video qui en n°1 et l'audio sera récupéré" idURL="principale" funcUrl={this.funcUrl}/>:null}
					              {(!this.state.clickSuivant2)?<InputModalVideoFu title="La video secondaire" titleMuted="Càd la video qui en n°2" idURL="secondaire" funcUrl={this.funcUrl}/>:null}
					            </div>
					            <div className="d-flex justify-content-end" style={{padding:"1rem"}}>
					            	{(this.checkUrl())?(!this.state.clickSuivant)?<button className="btn btn-outline-success" onClick={this.onClick}>Suivant</button>:null:null}
					            </div>
					            {(this.checkUrl())?(this.state.clickSuivant && !this.state.clickSuivant2)?<ChooseModalVideoFu funcChoixFiltre={this.funcChoixFiltre} />:null:null}
					            {(this.checkUrl())?(this.state.clickSuivant && !this.state.clickSuivant2)?suivant:null:null}
					            {(this.state.clickSuivant2)?<LancerModalVideoFu url={this.state.url} filtre={this.state.choixFiltre}/>:null}
			          		</div>

			        	</div>
			    	</div>
				</div>
    	)

	}
}

ReactDOM.render(
	<ModalVideoFu/>,
	document.getElementById('modalVideoFu')
);