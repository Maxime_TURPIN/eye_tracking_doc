
/*class ScenarioRangePosition extends React.Component{
	render(){
		return(
			
		)
	}
}*/

class ScenarioRangeInputPosition extends React.Component{
	constructor(props) {
	    super(props);
	    this.state = { 
	    	pourcentWidth: 10,
	    	pourcentHeight: 10,
	    	pixelWidth: 256,
	    	pixelHeight: 144,  
	    	pourcentX: 0,
	    	pourcentY: 0,
	    	pixelX: 0,
	    	pixelY: 0,
	    	url:null, 
	    };
  	}

  	hangleInputSelecteAll= (e) => {
  		e.target.select();
  	};

  	handleChangeWidth= (e) => {
  		if(e.target.value=="" || e.target.value>100 || e.target.value<0){return;}
  		this.setState({
  			pourcentWidth: e.target.value,
  			pixelWidth: e.target.value*2560/100
  		})
  	};

  	handleChangeHeight= (e) => {
  		if(e.target.value=="" || e.target.value>100 || e.target.value<0){return;}
  		this.setState({
  			pourcentHeight: e.target.value,
  			pixelHeight: e.target.value*1440/100
  		})
  	};

  	handleChangeX= (e) => {
  		if(e.target.value=="" || e.target.value>100 || e.target.value<0){return;}
  		this.setState({
  			pourcentX: e.target.value,
  			pixelX: e.target.value*2560/100
  		})
  	};

  	handleChangeY= (e) => {
  		if(e.target.value=="" || e.target.value>100 || e.target.value<0){return;}
  		this.setState({
  			pourcentY: e.target.value,
  			pixelY: e.target.value*1440/100
  		})
  	};

  	handleChangeWidthPixel= (e) => {
  		if(e.target.value=="" || e.target.value>2560 || e.target.value<0){return;}
  		this.setState({
  			pourcentWidth: e.target.value*100/2560,
  			pixelWidth: e.target.value
  		})
  	};

  	handleChangeHeightPixel= (e) => {
  		if(e.target.value=="" || e.target.value>1440 || e.target.value<0){return;}
  		this.setState({
  			pourcentHeight: e.target.value*100/1440,
  			pixelHeight: e.target.value
  		})
  	};

  	handleChangeXPixel= (e) => {
  		if(e.target.value=="" || e.target.value>2560 || e.target.value<0){return;}
  		this.setState({
  			pourcentX: e.target.value*100/2560,
  			pixelX: e.target.value
  		})
  	};

  	handleChangeYPixel= (e) => {
  		if(e.target.value=="" || e.target.value>1440 || e.target.value<0){return;}
  		this.setState({
  			pourcentY: e.target.value*100/1440,
  			pixelY: e.target.value
  		})
  	};

  	onClickButtonUrl=(e)=>{
		(this.state.interval!==null)?clearInterval(this.state.interval):null;
		const { dialog } = require('electron').remote;
		var ext = null;
		(this.props.typeRangeScenario2=="vidéo")?ext={ name: 'video', extensions: ['mp4','webm','ogg']}:ext={ name: 'image', extensions: ['jpeg','jpg','gif','png','apng','svg','bpm','ico','webp'] };
        var file = dialog.showOpenDialog({ properties: ['openFile'],filters: [
            ext
        ]});
        (file!==undefined)?this.setState({url:file}):null;
    }

	render(){
		return(
			<div>
			<div className="row no-gutters">
			<div className="col-md-5">
			<svg className="bd-placeholder-img" width="384" height="216" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image"><title>Placeholder</title><rect width="100%" height="100%" fill="#343A40"></rect><rect width={this.state.pourcentWidth+"%"} height={this.state.pourcentHeight+"%"} x={this.state.pourcentX+"%"} y={this.state.pourcentY+"%"} fill="#6d7fcc" ></rect></svg>
			</div>
			<div className="col-md-7">
			<div className="card-body">
			<div className="form-group">
			<label>{this.props.typeRangeScenario}</label>
            <button type="button" className={"btn btn-outline-success btn-block storage"} onClick={this.onClickButtonUrl} name={"url "+this.props.typeRangeScenario2+" "+this.props.numSalle} value={this.state.url}><svg xmlns="http://www.w3.org/2000/svg" width="20" viewBox="0 0 8 8" ><path d="M0 0v8h7v-4h-4v-4h-3zm4 0v3h3l-3-3z" /></svg>{(this.state.url===null)?"  Aucun fichier choisi":"  "+this.state.url.toString()}</button>
			<div className="row">
			<div className="col">
			<label >Position de la {this.props.typeRangeScenario2} en X</label>
			<input type="range" className="custom-range" value={this.state.pourcentX} min="0" max="100" onChange={this.handleChangeX}/>

			<label >Largeur de la {this.props.typeRangeScenario2}</label>
			<input type="range" className="custom-range" value={this.state.pourcentWidth} min="0" max="100" onChange={this.handleChangeWidth}/>
			</div>
			<div className="col">
			<label >Position de la {this.props.typeRangeScenario2} en Y</label>
			<input type="range" className="custom-range" value={this.state.pourcentY} min="0" max="100" onChange={this.handleChangeY}/>

			<label >Longueur de la {this.props.typeRangeScenario2}</label>
			<input type="range" className="custom-range" value={this.state.pourcentHeight} min="0" max="100" onChange={this.handleChangeHeight}/>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
			<div className="card-body">
			<h5 className="card-title">En pixel</h5>
			<div className="row">
			<div className="col">
			<label>x :</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control storage"  name={"pixel_X "+this.props.typeRangeScenario2+" "+this.props.numSalle} value={this.state.pixelX} step="0.00001" min="0" max="2560" onChange={this.handleChangeXPixel} onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">px</span>
			</div>
			</div>
			<label>Largeur</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control storage"  name={"pixel_Width "+this.props.typeRangeScenario2+" "+this.props.numSalle} value={this.state.pixelWidth} step="0.00001" min="0" max="2560" onChange={this.handleChangeWidthPixel} onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">px</span>
			</div>
			</div>
			</div>
			<div className="col">
			<label>y :</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control storage"  name={"pixel_Y "+this.props.typeRangeScenario2+" "+this.props.numSalle} value={this.state.pixelY} step="0.00001" min="0" max="1440" onChange={this.handleChangeYPixel} onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">px</span>
			</div>
			</div>
			<label>Longueur</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control storage"  name={"pixel_Height "+this.props.typeRangeScenario2+" "+this.props.numSalle} value={this.state.pixelHeight} step="0.00001" min="0" max="1440" onChange={this.handleChangeHeightPixel} onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">px</span>
			</div>
			</div>
			</div>
			</div>
			<h5 className="card-title">En %</h5>
			<div className="row">
			<div className="col">
			<label>x :</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control" value={this.state.pourcentX} step="0.00001" min="0" max="100" onChange={this.handleChangeX} onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">%</span>
			</div>
			</div>
			<label>Largeur</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control" value={this.state.pourcentWidth} step="0.00001" min="0" max="100" onChange={this.handleChangeWidth} onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">%</span>
			</div>
			</div>
			</div>
			<div className="col">
			<label>y :</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control" value={this.state.pourcentY} step="0.00001" min="0" max="100" onChange={this.handleChangeY}  onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">%</span>
			</div>
			</div>
			<label>Longueur</label>
			<div className="input-group mb-3">
			<input type="number" className="form-control" value={this.state.pourcentHeight} step="0.00001" min="0" max="100" onChange={this.handleChangeHeight}  onClick={this.hangleInputSelecteAll}/>
			<div className="input-group-append">
			<span className="input-group-text">%</span>
			</div>
			</div>
			</div>
			</div>
			<h5 className="card-title">Réglage dans le temps</h5>
			<div className="row">
			<div className="col">
			<label>Durée (en ms)</label>
			<input type="number" className="form-control storage" name={"duree "+this.props.typeRangeScenario2+" "+this.props.numSalle} defaultValue="1000" step="1" onClick={this.hangleInputSelecteAll}/>
			</div>
			<div className="col">
			<label>A quel moment ? (en ms)</label>
			<input type="number" className="form-control storage" name={"moment "+this.props.typeRangeScenario2+" "+this.props.numSalle} defaultValue="500" step="1" onClick={this.hangleInputSelecteAll}/>
			</div>
			</div>
			</div>
			</div>
		)
	}
}

class ButtonAdd extends React.Component{	
	render(){	
		return(
			<button type="button" className="btn btn-outline-primary btn-lg btn-block" onClick={this.props.clickButtonAdd}>Ajouter une {this.props.typeRangeScenario2} suplémentaire</button>
		)			
	}
}

class ButtonSuppr extends React.Component{
	render(){
		return(
			<button type="button" className="btn btn-outline-danger btn-lg btn-block" onClick={this.props.clickButtonSuppr}>Suppr une {this.props.typeRangeScenario2}</button>
		)
	}
}



class ScenarioPos extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			nbrEle:1,
		}
	}

	handleAdd=(e)=>{
		this.setState({
			nbrEle: this.state.nbrEle+1
		})
	};

	handleSuppr=(e)=>{
		this.setState({
			nbrEle: this.state.nbrEle-1
		})
	};

	render(){
		var ele = [];
		for (var i = 1; i <= this.state.nbrEle; i++) {
			ele.push(<div className="card animated zoomIn fasted" key={"div"+i}><ScenarioRangeInputPosition typeRangeScenario={this.props.typeRangeScenario} typeRangeScenario2={this.props.typeRangeScenario2} numSalle={"salle"+this.props.numSalle} key={"ScenarioRangeInputPosition"+i}/></div>);
			ele.push(<br key={"br"+i}/>)
		}

		return(
			<div>
				{ele}
				{this.state.nbrEle>1? <ButtonSuppr clickButtonSuppr={this.handleSuppr} typeRangeScenario2={this.props.typeRangeScenario2}/>: null}
				<ButtonAdd clickButtonAdd={this.handleAdd} typeRangeScenario2={this.props.typeRangeScenario2}/>
			</div>
		)
	}
}

class ScenarioVideo extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			addVideo: true,
		}
	}

	handleSwith= (e)=>{
		var i = this.state.addVideo;
		(i)?i=false:i=true;
		this.setState({
			addVideo: i
		})
	}


	render(){
		return(
			<div>
				<h5 className="card-title">Video</h5>
				<div className="custom-control custom-switch">
        			<input type="checkbox" className="custom-control-input" id={"SwitchVideo"+this.props.numSalle} defaultChecked onClick={this.handleSwith}/>
          			<label className="custom-control-label" htmlFor={"SwitchVideo"+this.props.numSalle}>Ajouter une video</label>
          		</div>
          		{this.state.addVideo? <ScenarioPos typeRangeScenario="Video" typeRangeScenario2="vidéo" numSalle={this.props.numSalle} />: null}
			</div>    
		)
	}
}

class ScenarioImage extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			addImage: true,
		}
	}

	handleSwith= (e)=>{
		var i = this.state.addImage;
		(i)?i=false:i=true;
		this.setState({
			addImage: i
		})
	}


	render(){
		return(
			<div>
				<h5 className="card-title">Image</h5>
				<div className="custom-control custom-switch">
        			<input type="checkbox" className="custom-control-input" id={"SwitchImage"+this.props.numSalle} defaultChecked onClick={this.handleSwith}/>
          			<label className="custom-control-label" htmlFor={"SwitchImage"+this.props.numSalle}>Ajouter une image</label>
          		</div>
          		{this.state.addImage? <ScenarioPos typeRangeScenario="Image" typeRangeScenario2="image" numSalle={this.props.numSalle} />: null}
			</div>    
		)
	}
}

class ScenarioHtmlDiffusion extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			pts: 0,
		}
	}

	handleChange=(e)=>{
		this.setState({pts:e.target.value})
	}

	render(){

		return(
			<div className={this.props.animate[this.props.idKey]}>
				<h5 className="card-title">Diffusion</h5>
				<div>
					<div className="card">
						<div className="card-body">
							<label className="my-1 mr-2">{"Diffuser sur l'écran de la salle "+this.props.numSalle}</label>
							<select className="custom-select my-1 mr-sm-2 storage" name={"select diffusion salle"+this.props.numSalle}>
								<option selected>--</option>
								<option value="cameraSalle1">la camera de la salle 1</option>
								<option value="cameraSalle2">la camera de la salle 2</option>
								<option value="cameraSalle3">la camera de la salle 3</option>
							</select>
							<div className="row">
								<div className="col">
									<div className="card">
										<div className="card-body">
											<label>Durée (en ms)</label>
											<input type="number" className={this.props.DisaDureeAndButtADD?"form-control":"form-control storage"} name={"duree diffusion salle"+this.props.numSalle} defaultValue="1000" value={this.props.DisaDureeAndButtADD?"":null} step="1" disabled={this.props.DisaDureeAndButtADD}/>
										</div>
									</div>
								</div>
								<div className="col">
									<label>A quel moment ? (en ms)</label>
									<input type="number" className="form-control storage" name={"moment diffusion salle"+this.props.numSalle} defaultValue="500" step="1"/>
								</div>
							</div>
							<label>Une différence de {this.state.pts} milliseconde</label>
							<input type="range" className="custom-range storage" name={"différence diffusion salle"+this.props.numSalle} defaultValue="0" min="0" max="30000" step="1" onChange={this.handleChange} />
						</div>
					</div> 
				</div>
			</div>
		)
	}
}

class ScenarioDiffusion extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			nbr: 1,
			idToggle: ["test","test1"],
			animate: ["animated zoomIn fast","animated zoomIn fast"],
			nbrTimeout: 0,
			DisaDureeAndButtADD: false,
		}
	}

	handleDisabledDureeAndButtADD=(e)=>{
		var tempo;
		(this.state.DisaDureeAndButtADD) ? tempo=false :tempo=true;
		this.setState({DisaDureeAndButtADD:tempo})
	};

	handleClickAdd=(e)=>{
		this.setState(state =>{
			const idToggle = state.idToggle.concat("test"+ (state.nbr+1))
			const animate = state.animate.concat("animated zoomIn fast")

			return{
				idToggle,
				nbr: state.nbr + 1,
				animate,
			};
		})

	};

	handleClickSuppr=(e)=>{
		if(this.state.nbrTimeout == 0){
			this.setState(state =>{
				const idToggle = state.idToggle.slice()
				const EleSuppr = idToggle.pop()

				const animate = state.animate.slice();
				animate[animate.length-1] = "animated zoomOut fast";

				const nbrTimeout = setTimeout(()=>{
					this.setState(state =>{
						const animate = state.animate.slice();
						const EleSuppr = animate.pop();

						return{
							animate,
							nbr: state.nbr - 1,
							nbrTimeout: 0,
						};
					})
				},500)

				return{
					idToggle,
					animate,
					nbrTimeout,
				};
			})
		}

	};

	render(){
		var scenarios = []

		for (var i = 1; i < this.state.nbr+1; i++) {
			var scenario = <ScenarioHtmlDiffusion idToggle={this.state.idToggle} nbr={this.state.nbr} idKey={i} animate={this.state.animate} DisaDureeAndButtADD={this.state.DisaDureeAndButtADD} handleDisabledDureeAndButtADD={this.handleDisabledDureeAndButtADD} numSalle={this.props.numSalle}/>;
			scenarios.push(scenario);
		}

		return(
			<div>
				{scenarios}
				{this.state.nbr>1? <ButtonSuppr clickButtonSuppr={this.handleClickSuppr} typeRangeScenario2="diffusion"/>: null}
				{this.state.DisaDureeAndButtADD? null : <ButtonAdd clickButtonAdd={this.handleClickAdd} typeRangeScenario2="diffusion"/>}
			</div>
		)
	}
}

class ScenarioSalle extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			down: true,
		};
	}

	handleClick=(e)=>{
		var tempo;
		this.state.down? tempo=false:tempo=true;
		this.setState({down:tempo})
	}

	render(){
		var chevronBottom = <path d="M1.5 1l-1.5 1.5 4 4 4-4-1.5-1.5-2.5 2.5-2.5-2.5z"></path>
		var chevronRight = <path d="M2.5 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z"></path>
		return(
			<div className="card">
                <h5 className="card-header" onClick={this.handleClick}><svg width="15px" viewBox="0 0 8 8" className={"animated"+(this.state.down?" rotateInDownLeft ":" rotateInDownRight ")+"slow"}>{this.state.down?chevronBottom:chevronRight}</svg> {" Scénario pour la salle "+this.props.numSalle}</h5>
                <div className="card-body" hidden={!this.state.down}>
                	<ScenarioVideo numSalle={this.props.numSalle}/>
                	<ScenarioImage numSalle={this.props.numSalle}/>
                	<ScenarioDiffusion numSalle={this.props.numSalle}/>
                	<div className='storage' name="fin" defaultValue=""></div>
                </div>
            </div>


		)
	}
}

ReactDOM.render(
	<ScenarioSalle numSalle="1" />,
	document.getElementById('scenarioSalle1')
);

ReactDOM.render(
	<ScenarioSalle numSalle="2" />,
	document.getElementById('scenarioSalle2')
);

ReactDOM.render(
	<ScenarioSalle numSalle="3" />,
	document.getElementById('scenarioSalle3')
);