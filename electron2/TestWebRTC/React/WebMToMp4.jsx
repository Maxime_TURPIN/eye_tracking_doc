class WebMToMp4 extends React.Component{

	constructor(props){
		super(props)
		this.state= {
			time1:0,
			ariaValuenow:0,
			width:{width: "0%"},
			start:null,
			lancer:false,
			class_:"",
		}
	}

	lancer=()=>{
		this.setState({lancer:true})
		const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
      	const ffmpeg = require('fluent-ffmpeg');
      	ffmpeg.setFfmpegPath(ffmpegPath);

		var tempo;
		var command = ffmpeg(this.props.urlVideoWebm+'.webm').format('mp4').save(this.props.urlVideoMp4+'.mp4').on('start',function(){
        	this.setState({time1:Date.now(),start:true})
      	}.bind(this)).on('end',function(){
          	this.setState({ariaValuenow:100,width:{width: "100%"},start:false})
          	iiiiiiii_.pop()
      	}.bind(this));
	};

	render(){
		(this.state.lancer===false)?this.lancer():null;
		var spinner = 	<div className="spinner-border text-primary" role="status">
						  	<span className="sr-only">Loading...</span>
						</div>;

		return(
			<React.Fragment>
				<hr/>
				<div className="modal-body">
					<div className="d-flex justify-content-center">
						{(this.state.ariaValuenow!=100)?spinner:null}
					</div>

	              <div>
	                <p>{this.props.urlVideoMp4+'.mp4'}</p>
	              </div>

	              <div className="progress">
	                <div className="progress-bar progress-bar-animated" role="progressbar" aria-valuenow={this.state.ariaValuenow} aria-valuemin="0" aria-valuemax="100" style={this.state.width}></div>
	              </div>
	            </div>
	             <hr size="30"/>
	        </React.Fragment>
		)
	}
}

class ModalWebMToMp4 extends React.Component{

	constructor(props){
		super(props)
		this.state= {
			progressBar:[],
			setInterval_:null,
			openFolder:false
		}
	}

	progressBar=(data)=>{
		(data.length !== this.state.progressBar.length)?this.setState({progressBar:data}):null;
	}


	render(){
		var progressBar=[];
		(this.state.setInterval_===null)?this.setState({setInterval_:setInterval(()=>{
			for(var i = 0; i < iiiiiiii_.length;i++){
				(progressBar.length<6)?progressBar.push(<WebMToMp4 urlVideoWebm={iiiiiiii_[i].urlVideoWebm} urlVideoMp4={iiiiiiii_[i].urlVideoMp4} key={i} id={i}/>):null;
			}
			if(iiiiiiii_.length===0){
				progressBar=[];this.progressBar(progressBar);
				if(this.state.openFolder){
					$('#ModalWebMToMp4').modal('hide');
					const {shell} = require('electron'); // deconstructing assignment
					shell.openItem(fileSave);
					this.setState({openFolder:false})
				}
			}
			if(iiiiiiii_.length!==0){
				this.progressBar(progressBar)
			}
			(progressBar.length==6)?this.setState({openFolder:true}):null;
		},2000)}):null;
		return(
			<div className="modal fade" tabindex="-1" id="ModalWebMToMp4" role="dialog" aria-labelledby="ModalWebMToMp4" aria-hidden="true">
			  <div className="modal-dialog modal-xl">
			    <div className="modal-content">
			    	<div className="modal-header">
					    <h5 className="modal-title" >WebM to Mp4</h5>
					</div>
			      	{this.state.progressBar}
			    </div>
			  </div>
			</div>
		)
	}
}

ReactDOM.render(
	<ModalWebMToMp4/>,
	document.getElementById('ModalWebMToMp4_')
);