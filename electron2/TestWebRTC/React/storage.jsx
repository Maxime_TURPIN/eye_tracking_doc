class Storage extends React.Component{

	constructor(props){
		super(props)
		this.state= {
			success:null,
		}
	}

	handleClick=(e)=>{
		this.setState({success:
				<div className="alert alert-success alert-dismissible fade show" role="alert">
				  <strong>Sauvegarder!</strong>
				  <button type="button" className="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>})
		setTimeout(()=>{this.setState({success:null})},5000)
		var x = document.getElementById(this.props.id).getElementsByClassName("storage");
		var date =moment().format('DD_MM_YYYY_h_mm_ss');
		var video=[];
		var nomScenario=document.getElementById('nameScenario').value
		console.log(x.length)
		//console.log(x)
		console.log("----------------------------------------------------------------------")
		for (var i =0; i<x.length;i++){
			video.push([x[i].name,x[i].value])
		}
		var json = JSON.stringify({date,video,nomScenario})
		const storage = require('electron-json-storage');
		storage.setDataPath(__dirname+"/../Storage/scenar");
		(this.props.fileName===false)?storage.set(nomScenario+"____"+date, json, function(error){
	  		if (error) throw error;
		}):storage.set(this.props.fileName, json, function(error){
	  		if (error) throw error;
		});
		console.log(json)
	}
	render(){
		return(
			<React.Fragment>
				<div className="clearfix">
            		<button type="submit" className="btn btn-outline-success float-right" onClick={this.handleClick}>Suivant</button>
        		</div>
        		<br/>
        		{this.state.success}
        	</React.Fragment>		
       	)
	}
}




ReactDOM.render(
	<Storage id="scenario" fileName={false}/>,
	document.getElementById('scenarioStorage')
);