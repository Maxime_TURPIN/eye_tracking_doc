/*
*  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

'use strict';

const videoElement3 = document.getElementById('video3');
const audioInputSelect3 = document.querySelector('select#audioSource3');
const audioOutputSelect3 = document.querySelector('select#audioOutput3');
const videoSelect3 = document.querySelector('select#videoSource3');
const selectors3 = [audioInputSelect3, audioOutputSelect3, videoSelect3];

audioOutputSelect3.disabled = !('sinkId' in HTMLMediaElement.prototype);

function gotDevices3(deviceInfos) {
  // Handles being called several times to update labels. Preserve values3.
  const values3 = selectors3.map(select => select.value);
  selectors3.forEach(select => {
    while (select.firstChild) {
      select.removeChild(select.firstChild);
    }
  });
  for (let i = 0; i !== deviceInfos.length; ++i) {
    const deviceInfo3 = deviceInfos[i];
    const option3 = document.createElement('option');
    option3.value = deviceInfo3.deviceId;
    if (deviceInfo3.kind === 'audioinput') {
      option3.text = deviceInfo3.label || `microphone ${audioInputSelect3.length + 1}`;
      audioInputSelect3.appendChild(option3);
    } else if (deviceInfo3.kind === 'audiooutput') {
      option3.text = deviceInfo3.label || `speaker ${audioOutputSelect3.length + 1}`;
      audioOutputSelect3.appendChild(option3);
    } else if (deviceInfo3.kind === 'videoinput') {
      option3.text = deviceInfo3.label || `camera ${videoSelect3.length + 1}`;
      videoSelect3.appendChild(option3);
    } else {
      console.log('Some other kind of source/device: ', deviceInfo3);
    }
  }
  selectors3.forEach((select, selectorIndex) => {
    if (Array.prototype.slice.call(select.childNodes).some(n => n.value === values3[selectorIndex])) {
      select.value = values3[selectorIndex];
    }
  });
}

navigator.mediaDevices.enumerateDevices().then(gotDevices3).catch(handleError3);

// Attach audio output device to video element using device/sink ID.
function attachSinkId3(element, sinkId) {
  if (typeof element.sinkId !== 'undefined') {
    element.setSinkId(sinkId)
      .then(() => {
        console.log(`Success, audio output device attached: ${sinkId}`);
      })
      .catch(error => {
        let errorMessage = error;
        if (error.name === 'SecurityError') {
          errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
        }
        console.error(errorMessage);
        // Jump back to first output device in the list as it's the default.
        audioOutputSelect3.selectedIndex = 0;
      });
  } else {
    console.warn('Browser does not support output device selection.');
  }
}

function changeaudioDestination3() {
  const audioDestination3 = audioOutputSelect3.value;
  attachSinkId3(videoElement3, audioDestination3);
}

function gotStream3(stream) {
  window.stream_salle3 = stream; // make stream available to console
  videoElement3.srcObject = stream;
  // Refresh button list in case labels have become available
  return navigator.mediaDevices.enumerateDevices();
}

function handleError3(error) {
  console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

function start3() {
  if (window.stream_salle3) {
    window.stream_salle3.getTracks().forEach(track => {
      track.stop();
    });
  }
  const audioSource3 = audioInputSelect3.value;
  const videoSource3 = videoSelect3.value;
  //console.log("=======>3",videoSelect3.value);
  const constraints3 = {
    audio: {deviceId: audioSource3 ? {exact: audioSource3} : undefined},
    video: {deviceId: videoSource3 ? {exact: videoSource3} : undefined,width:  1920, height:  1080}
  };
  navigator.mediaDevices.getUserMedia(constraints3).then(gotStream3).then(gotDevices3).catch(handleError3);
}

audioInputSelect3.onchange = start3;
audioOutputSelect3.onchange = changeaudioDestination3;

videoSelect3.onchange = start3;

start3();
