/*
*  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

'use strict';

const videoElement2 = document.getElementById('video2');
const audioInputSelect2 = document.querySelector('select#audioSource2');
const audioOutputSelect2 = document.querySelector('select#audioOutput2');
const videoSelect2 = document.querySelector('select#videoSource2');
const selectors2 = [audioInputSelect2, audioOutputSelect2, videoSelect2];

audioOutputSelect2.disabled = !('sinkId' in HTMLMediaElement.prototype);

function gotDevices2(deviceInfos) {
  // Handles being called several times to update labels. Preserve values2.
  const values2 = selectors2.map(select => select.value);
  selectors2.forEach(select => {
    while (select.firstChild) {
      select.removeChild(select.firstChild);
    }
  });
  for (let i = 0; i !== deviceInfos.length; ++i) {
    const deviceInfo2 = deviceInfos[i];
    const option2 = document.createElement('option'); option2.value = deviceInfo2.deviceId;
    if (deviceInfo2.kind === 'audioinput') {
      option2.text = deviceInfo2.label || `microphone ${audioInputSelect2.length + 1}`;
      audioInputSelect2.appendChild(option2);
    } else if (deviceInfo2.kind === 'audiooutput') {
      option2.text = deviceInfo2.label || `speaker ${audioOutputSelect2.length + 1}`;
      audioOutputSelect2.appendChild(option2);
    } else if (deviceInfo2.kind === 'videoinput') {
      option2.text = deviceInfo2.label || `camera ${videoSelect2.length + 1}`;
      videoSelect2.appendChild(option2);
    } else {
      console.log('Some other kind of source/device: ', deviceInfo2);
    }
  }
  selectors2.forEach((select, selectorIndex) => {
    if (Array.prototype.slice.call(select.childNodes).some(n => n.value === values2[selectorIndex])) {
      select.value = values2[selectorIndex];
    }
  });
}

navigator.mediaDevices.enumerateDevices().then(gotDevices2).catch(handleError2);

// Attach audio output device to video element using device/sink ID.
function attachSinkId2(element, sinkId) {
  if (typeof element.sinkId !== 'undefined') {
    element.setSinkId(sinkId)
      .then(() => {
        console.log(`Success, audio output device attached: ${sinkId}`);
      })
      .catch(error => {
        let errorMessage = error;
        if (error.name === 'SecurityError') {
          errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
        }
        console.error(errorMessage);
        // Jump back to first output device in the list as it's the default.
        audioOutputSelect2.selectedIndex = 0;
      });
  } else {
    console.warn('Browser does not support output device selection.');
  }
}

function changeaudioDestination2() {
  const audioDestination2 = audioOutputSelect2.value;
  attachSinkId2(videoElement2, audioDestination2);
}

function gotStream2(stream) {
  window.stream_salle2 = stream; // make stream available to console
  videoElement2.srcObject = stream;
  // Refresh button list in case labels have become available
  return navigator.mediaDevices.enumerateDevices();
}

function handleError2(error) {
  console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

function start2() {
  if (window.stream_salle2) {
    window.stream_salle2.getTracks().forEach(track => {
      track.stop();
    });
  }
  const audioSource2 = audioInputSelect2.value;
  const videoSource2 = videoSelect2.value;
  //console.log("=======>2",videoSelect.value);
  const constraints2 = {
    audio: {deviceId: audioSource2 ? {exact: audioSource2} : undefined},
    video: {deviceId: videoSource2 ? {exact: videoSource2} : undefined,width:  1920, height:  1080}
  };
  navigator.mediaDevices.getUserMedia(constraints2).then(gotStream2).then(gotDevices2).catch(handleError2);
}

audioInputSelect2.onchange = start2;
audioOutputSelect2.onchange = changeaudioDestination2;

videoSelect2.onchange = start2;

start2();
