$('#scenario').load('./scenario.html')
$('#lesScenarios').load('./LesScenarios.html')

var Aff = ["#home","#scenario","#lesScenarios"];

function hideAff(){
	Aff.forEach(function(item, index, array){
		$(item).hide()
	})
}

function AffScenario(){
	hideAff()
	$('#scenario').show()
	$('li.active').removeClass('active')
	$('#liScenario').addClass('active')
}

$('li#liScenario').click(AffScenario)

function AffLesScenarios(){
	hideAff()
	$('#lesScenarios').show()
	$('li.active').removeClass('active')
	$('#liLesScenarios').addClass('active')
}

$('li#liLesScenarios').click(AffLesScenarios)

function AffHome(){
	hideAff()
	$('#home').show()
	$('li.active').removeClass('active')
	$('#liHome').addClass('active')
}
//On affiche le home car on commence forcement par le home
$('#home').load('./home.html') 
AffHome()

$('li#liHome').click(AffHome)
