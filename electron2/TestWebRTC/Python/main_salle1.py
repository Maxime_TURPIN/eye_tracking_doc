import time
import tobii_research as tr
#from socketIO_client import SocketIO,LoggingNamespace
import socketio
import logging
import csv
import random
from tobii_research_addons import ScreenBasedCalibrationValidation, Point2

sio_ = socketio.Client()
sio_.connect('http://localhost:80')
sio = socketio.Client()

# sio = SocketIO('localhost', 8081, LoggingNamespace)
global global_gaze_data_buff
global_gaze_data_buff = []

aff=0

# def on_aaa_response(*args):
#     print('--------------------------------------------------------')
#     print('on_aaa_response', args)


# sio_.on('testClaibration', on_aaa_response)
print("yes")
@sio_.on('TrackerIdSalle1')
def on_message(data):
    print("data['url']:")
    print(data['url'])
    choix(data)

@sio_.on('gaze_dataSalle1')
def on_message(data):
    gaze_data()

@sio_.on('stop_gaze_dataSalle1')
def on_message(data):
    stop_data(data)

# @sio_.on('file_present')
# def on_message(data):
#     global aff
#     aff+=1

# @sio_.on('file_present_fin')
# def on_message(data):
#     global aff
#     aff-=1

def gaze_data_callback(gaze_data):
    #global_gaze_data_buff.append(aff)
    global_gaze_data_buff.append(gaze_data)

def gaze_data():
    #print("Subscribing to gaze data for eye tracker with serial number {0}.".format(eyetracker.serial_number))
    tracker.subscribe_to(tr.EYETRACKER_GAZE_DATA, gaze_data_callback, as_dictionary=True)


def stop_data(data):
    tracker.unsubscribe_from(tr.EYETRACKER_GAZE_DATA, gaze_data_callback)
    write_data_tsv(data)
    print("Unsubscribed from gaze data.")

def write_data_tsv(url):
    with open(url['url']+'\\tracker_salle1.tsv', 'wt') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        # gaze_data_buff = global_gaze_data_buff
        # global_gaze_data_buff = []
        tab=[]
        for data_buff in global_gaze_data_buff[0]:
            #print("{0}:{1}",data_buff,type(global_gaze_data_buff[0][data_buff]))
            if isinstance(global_gaze_data_buff[0][data_buff],tuple) and len(global_gaze_data_buff[0][data_buff])>1:
                tab.append(data_buff+'_x')
                tab.append(data_buff+'_y')
                if len(global_gaze_data_buff[0][data_buff])>=3:
                    tab.append(data_buff+'_z')
            else:
                tab.append(data_buff)
            

        tsv_writer.writerow(tab)
        for data_buff in global_gaze_data_buff:
            tab=[]
            for data_buff_ in data_buff:
                if isinstance(data_buff[data_buff_],tuple):
                    for xyz in data_buff[data_buff_]:
                        tab.append(xyz)
                else:
                    tab.append(data_buff[data_buff_])
            tsv_writer.writerow(tab)

def choix(data):
    eyetrackers = tr.find_all_eyetrackers()
    oui=''
    print(data['tr'])
    for eyetracker in eyetrackers:
        if eyetracker.serial_number==data['tr']:
            oui=eyetracker
    if oui=='':
        sio_.emit('searchEyetrackerSalle1',{'eyetrackerTrue': False})
    else:
        sio_.emit('searchEyetrackerSalle1',{'eyetrackerTrue': True})
        global tracker
        tracker=oui
        print(tracker)
        calibration(oui,data)

#test=choix()
#gaze_data(test)

def calibration(eyetracker,data):
        time.sleep(5)
        sio.connect('http://localhost:8081')
        if eyetracker is None:
            return

        # <BeginExample>


        calibration = tr.ScreenBasedCalibration(eyetracker)

        # Enter calibration mode.
        calibration.enter_calibration_mode()
        print("Entered calibration mode for eye tracker with serial number {0}.".format(eyetracker.serial_number))

        # Define the points on screen we should calibrate at.
        # The coordinates are normalized, i.e. (0.0, 0.0) is the upper left corner and (1.0, 1.0) is the lower right corner.
        points_to_calibrate = [(0.1, 0.1),(0.5, 0.1),(0.9, 0.1),(0.1, 0.5),(0.5, 0.5),(0.9, 0.5),(0.1, 0.9),(0.5, 0.9),(0.9, 0.9)]
        if data['ptsCali']==3:
            points_to_calibrate = [(0.1, 0.1),(0.5, 0.5),(0.9, 0.9)]
        if data['ptsCali']==5:
            points_to_calibrate = [(0.1, 0.1),(0.9, 0.1),(0.5, 0.5),(0.1, 0.9),(0.9, 0.9)]
        random.shuffle(points_to_calibrate)
        sample_count = 30
        timeout_ms = 1000
        calibration_result2 = 0
        with ScreenBasedCalibrationValidation(eyetracker, sample_count, timeout_ms) as calib:
            for point in points_to_calibrate:
                print("Show a point on screen at {0}.".format(point))
                sio.emit('calibration',{'x':point[0],'y':point[1]})
                # Wait a little for user to focus.
                #time.sleep(1/120)
                calibration_status = 'calibration_status_failure'
                i =0
                print("Collecting data at {0}.".format(point))
                while(calibration_status == 'calibration_status_failure' or i <= 2) :
                    #if calibration.collect_data(point[0], point[1]) != tr.CALIBRATION_STATUS_SUCCESS:
                        # Try again if it didn't go well the first time.
                        # Not all eye tracker models will fail at this point, but instead fail on ComputeAndApply.
                    calibration.collect_data(point[0], point[1])
                    #print("Computing and applying calibration.")
                    calibration_result = calibration.compute_and_apply()
                    calibration_status = calibration_result.status
                    if calibration_status == 'calibration_status_failure':
                        print("Compute and apply returned {0} and collected at {1} points.".format(calibration_result.status,len(calibration_result.calibration_points)))
                        calibration.discard_data(point[0], point[1])
                        #time.sleep(1/120)
                        i=0
                    else:
                        if i < 1:
                            calibration.discard_data(point[0], point[1])
                        i+=1
                calib.start_collecting_data(Point2(point[0], point[1]))
                while calib.is_collecting_data:
                    time.sleep(0.5)
            calibration_result2 = calib.compute()
            #https://github.com/tobiipro/prosdk-addons-python/blob/master/tobii_research_addons/ScreenBasedCalibrationValidation.py
        print(calibration_result2.average_accuracy_left)
        filename = data['url']+'\\saved_calibration_salle1.csv'
        average_accuracy_left=["average_accuracy_left",calibration_result2.average_accuracy_left]
        average_accuracy_right=["average_accuracy_right",calibration_result2.average_accuracy_right]
        average_precision_left=["average_precision_left",calibration_result2.average_precision_left]
        average_precision_right=["average_precision_right",calibration_result2.average_precision_right]
        average_precision_rms_left=["average_precision_rms_left",calibration_result2.average_precision_rms_left]
        average_precision_rms_right=["average_precision_rms_right",calibration_result2.average_precision_rms_right]
        tempo=[average_accuracy_left,average_accuracy_right,average_precision_left,average_precision_right,average_precision_rms_left,average_precision_rms_right]
         # Save the calibration to file.
        with open(filename, "w") as f:
            csv_writer = csv.writer(f, delimiter='\t')
            for data_csv in tempo:
                csv_writer.writerow(data_csv)
           
        calibration.leave_calibration_mode()
        sio_.emit('leave_calibrationSalle1',{'leave_calibration':True})
        print("Left calibration mode.")
# calibration(test)