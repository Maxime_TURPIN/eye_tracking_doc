import time
import tobii_research as tr
#from socketIO_client import SocketIO,LoggingNamespace
import socketio
import logging
import csv


sio_ = socketio.Client()
sio_.connect('http://localhost:80')


def execute():
        # <BeginExample>
        eyetrackers = tr.find_all_eyetrackers()
        
        oui='oui'
        eyes=[]

        for eyetracker in eyetrackers:
            eye=[]
            print("Address: " + eyetracker.address)
            print("Model: " + eyetracker.model)
            print("Name (It's OK if this is empty): " + eyetracker.device_name)
            print("Serial number: " + eyetracker.serial_number)
            eye.append(eyetracker.device_name)
            eye.append(eyetracker.serial_number)
            eyes.append(eye)
            if eyetracker.serial_number=='X3120-030107906981':
                oui=eyetracker
        # <EndExample>

        sio_.emit('calibration',eyes)

execute()
