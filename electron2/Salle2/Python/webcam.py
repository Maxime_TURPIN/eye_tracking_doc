import numpy as np
import cv2
import time
import os
import sys
from multiprocessing import Process

cap = cv2.VideoCapture(int(sys.argv[6]))
cap.set(3,1920)
cap.set(4,1080)
cap.set(5,60)

delay = float(sys.argv[1])
img=[]
exit = float(sys.argv[2])
t0 = time.time()

while(True):
	ret, frame = cap.read()
	timestamp = time.time()
	if sys.argv[3]=='miroir':
		img.append([frame,timestamp])
	else:
		img.append([cv2.flip(frame,1),timestamp])  #miroir:frame interact:cv2.flip(frame,1)
	# print(img[len(img)-1][1]-img[0][1])
	# Display the resulting frame
	# print(img[len(img)-1][1]-img[0][1])
	if((img[len(img)-1][1]-img[0][1])>=delay):
		img_ = img.pop(0)
		cv2.namedWindow(sys.argv[5], cv2.WND_PROP_FULLSCREEN)
		cv2.setWindowProperty(sys.argv[5],cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
		cv2.moveWindow(sys.argv[5],int(sys.argv[4])*2560,0)
		cv2.imshow(sys.argv[5],img_[0])
		cv2.waitKey(1)
	if (img[0][1] -t0) >= exit:
		break
	

# When everything done, release the capture
cap.release()
cv2.destroyAllWindow(sys.argv[5])