// Modules to control application life and create native browser window
const electron = require('electron')
const {app, BrowserWindow,ipcMain} = require('electron')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let WindowEcran1
let WindowEcran2
let WindowEcran3

let CalibrageSalle1
let CalibrageSalle2
let CalibrageSalle3


function createMainWindow () {
    //mainWindow = new BrowserWindow({skipTaskbar:false,autoHideMenuBar:false,maximizable:true})
    const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize
    mainWindow = new BrowserWindow({ width, height })
    mainWindow.maximize()
  

  // and load the index.html of the app.
  mainWindow.loadFile('TestWebRTC/index.html')

  // Open the DevTools.
  // WindowEcran1.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

function createWindow () {
  // Create the browser window.
  let displays = electron.screen.getAllDisplays()
  let externalDisplay = displays[1]

  if (externalDisplay) {
     WindowEcran1 = new BrowserWindow({
       x: externalDisplay.bounds.x + 50,
       y: externalDisplay.bounds.y + 50,
       skipTaskbar:true,
       autoHideMenuBar:true,
       parent:mainWindow,
     })
    //WindowEcran1 = new BrowserWindow({skipTaskbar:true,autoHideMenuBar:true})
    WindowEcran1.setFullScreen(true)
  }
  

  // and load the index.html of the app.
  WindowEcran1.loadFile('Salle1/index.html')

  // Open the DevTools.
  // WindowEcran1.webContents.openDevTools()

  // Emitted when the window is closed.
  WindowEcran1.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    WindowEcran1 = null
  })
}

function createWindow2 () {
  // Create the browser window.
  let displays2 = electron.screen.getAllDisplays()
  let  externalDisplay2= displays2[2]

  
     if (externalDisplay2) {
      WindowEcran2 = new BrowserWindow({
       x: externalDisplay2.bounds.x + 50,
       y: externalDisplay2.bounds.y + 50,
       skipTaskbar:true,
       autoHideMenuBar:true,
       parent:mainWindow,
      })
    //WindowEcran1 = new BrowserWindow({skipTaskbar:true,autoHideMenuBar:true})
      WindowEcran2.setFullScreen(true)
    }
  

  // and load the index.html of the app.
  WindowEcran2.loadFile('Salle2/index.html')

  // Open the DevTools.
  // WindowEcran1.webContents.openDevTools()

  // Emitted when the window is closed.
  WindowEcran2.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    WindowEcran2 = null
  })
}

function createWindow3 () {
  // Create the browser window.
  let displays3 = electron.screen.getAllDisplays()
  let  externalDisplay3= displays3[3]

  
     if (externalDisplay3) {
      WindowEcran3 = new BrowserWindow({
       x: externalDisplay3.bounds.x + 50,
       y: externalDisplay3.bounds.y + 50,
       skipTaskbar:true,
       autoHideMenuBar:true,
       parent:mainWindow,
      })
    //WindowEcran1 = new BrowserWindow({skipTaskbar:true,autoHideMenuBar:true})
      WindowEcran3.setFullScreen(true)
    }
  

  // and load the index.html of the app.
  WindowEcran3.loadFile('Salle3/index.html')

  // Open the DevTools.
  // WindowEcran1.webContents.openDevTools()

  // Emitted when the window is closed.
  WindowEcran3.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    WindowEcran3 = null
  })
}

function createCalibrageSalle1() {
  // Create the browser window.
  let displays4 = electron.screen.getAllDisplays()
  let  externalDisplay4= displays4[1]

  
     if (externalDisplay4) {
      CalibrageSalle1 = new BrowserWindow({
       x: externalDisplay4.bounds.x + 50,
       y: externalDisplay4.bounds.y + 50,
       skipTaskbar:true,
       autoHideMenuBar:true,
       parent:mainWindow,
      })
    //WindowEcran1 = new BrowserWindow({skipTaskbar:true,autoHideMenuBar:true})
      CalibrageSalle1.setFullScreen(true)
    }
  

  // and load the index.html of the app.
  CalibrageSalle1.loadFile('TestWebRTC/Calibrage/Salle1Cali/index.html')

  // Open the DevTools.
  // WindowEcran1.webContents.openDevTools()

  // Emitted when the window is closed.
  CalibrageSalle1.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    CalibrageSalle1 = null
  })
}

function createCalibrageSalle2() {
  // Create the browser window.
  let displays5 = electron.screen.getAllDisplays()
  let  externalDisplay5= displays5[2]

  
     if (externalDisplay5) {
      CalibrageSalle2 = new BrowserWindow({
       x: externalDisplay5.bounds.x + 50,
       y: externalDisplay5.bounds.y + 50,
       skipTaskbar:true,
       autoHideMenuBar:true,
       parent:mainWindow,
      })
    //WindowEcran1 = new BrowserWindow({skipTaskbar:true,autoHideMenuBar:true})
      CalibrageSalle2.setFullScreen(true)
    }
  

  // and load the index.html of the app.
  CalibrageSalle2.loadFile('TestWebRTC/Calibrage/Salle2Cali/index.html')

  // Open the DevTools.
  // WindowEcran1.webContents.openDevTools()

  // Emitted when the window is closed.
  CalibrageSalle2.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    CalibrageSalle2 = null
  })
}

function createCalibrageSalle3() {
  // Create the browser window.
  let displays6 = electron.screen.getAllDisplays()
  let  externalDisplay6= displays6[3]

  
     if (externalDisplay6) {
      CalibrageSalle3 = new BrowserWindow({
       x: externalDisplay6.bounds.x + 50,
       y: externalDisplay6.bounds.y + 50,
       skipTaskbar:true,
       autoHideMenuBar:true,
       parent:mainWindow,
      })
    //WindowEcran1 = new BrowserWindow({skipTaskbar:true,autoHideMenuBar:true})
      CalibrageSalle3.setFullScreen(true)
    }
  

  // and load the index.html of the app.
  CalibrageSalle3.loadFile('TestWebRTC/Calibrage/Salle3Cali/index.html')

  // Open the DevTools.
  // WindowEcran1.webContents.openDevTools()

  // Emitted when the window is closed.
  CalibrageSalle3.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    CalibrageSalle3 = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready',createMainWindow)
app.on('ready', createWindow)
app.on('ready',createWindow2)
app.on('ready',createWindow3)

ipcMain.on('minimize_Salle1', (event, arg) => {
  WindowEcran1.minimize()
})

ipcMain.on('restore_Salle1', (event, arg) => {
  WindowEcran1.restore()
})

ipcMain.on('minimize_Salle2', (event, arg) => {
  WindowEcran2.minimize()
})

ipcMain.on('restore_Salle2', (event, arg) => {
  WindowEcran2.restore()
})

ipcMain.on('minimize_Salle3', (event, arg) => {
  WindowEcran3.minimize()
})

ipcMain.on('restore_Salle3', (event, arg) => {
  WindowEcran3.restore()
})

ipcMain.on('open_CalibrageSalle1', (event, arg) => {
  createCalibrageSalle1()
})
ipcMain.on('close_CalibrageSalle1', (event, arg) => {
  CalibrageSalle1.close();
})

ipcMain.on('open_CalibrageSalle2', (event, arg) => {
  createCalibrageSalle2()
})
ipcMain.on('close_CalibrageSalle2', (event, arg) => {
  CalibrageSalle2.close();
})

ipcMain.on('open_CalibrageSalle3', (event, arg) => {
  createCalibrageSalle3()
})
ipcMain.on('close_CalibrageSalle3', (event, arg) => {
  CalibrageSalle3.close();
})


// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

/*app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (WindowEcran1 === null) createWindow()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (WindowEcran2 === null) createWindow()
})
app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (WindowEcran3 === null) createWindow()
})
app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})*/
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
