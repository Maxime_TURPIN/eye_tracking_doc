var latence = (latency_,id,id_2,id_audio,leftVideo_,timeout_)=>{
  var audioStream,videoStream,audioContexte,streamDestination,mainAudioNode,outputStream;
  var audioHtml = document.getElementById(id_audio)
  function createSimpleOutputStream(delay = undefined){
    /* en test : decompose le stream de la camera en deux stream*/
    audioStream = new MediaStream();

    //On creer un stream a partir du canvas
    var canvas=document.getElementById(id)

    var audioTracks = window.stream.getAudioTracks();
    /* recuperation les tracks du mainstream(en clair le son de la camera)
    Pareil, on s'en resert dans createSimpleOutputStream */
    audioTracks.forEach(function(track){
      audioStream.addTrack(track);
    });
    //creation d'un contexte audio web audio api --> permet d'appliquer des effets a un stream audio
    audioContexte = new (AudioContext || webkitAudioContext)();
    //Tricky ici, streamdestination permet de creer un stream sur lequel on peut "plugger" des effets audios
    streamDestination = audioContexte.createMediaStreamDestination();

    //Creation d'une source audio(le stream audio de la camera)
    mainAudioNode = audioContexte.createMediaStreamSource(audioStream);
    
    //Ajout d'un delay
    if(typeof delay!=="undefined"){
      var delayed = audioContexte.createDelay(delay*2);
      delayed.delayTime.value = delay;
      mainAudioNode.connect(delayed);
      //On ajoute le delay en sortie et dans le stream de destination
      //delayed.connect(audioContexte.destination);
      delayed.connect(streamDestination);
    }else{
      /* La fonction connect permet de connecter une source audio, 
      ici on ajoute le noeud audio principal
      (creer a partir de l'audiostream) a la sortie par defaut*/
      //mainAudioNode.connect(audioContexte.destination);
      mainAudioNode.connect(streamDestination);
    }
    //var processedAudioStream = streamDestination.stream;
    audioHtml.srcObject=streamDestination.stream
    attachSinkId(audioHtml, outputSourceSocketIO);
    Time2Element = window.performance.now()
    console.log("T2:"+Time2Element)
  }

  setTimeout(()=>{disconnectNormalAudio();}, timeout_);

  function disconnectNormalAudio(){
    audioContexte = null;
    mainAudioNode = null;
  }

  play();

  function play(){
    createSimpleOutputStream(latency_/1000)
  };

}
exports.latence = latence